﻿using HMS.Core;
using HMS.Core.DomainObjects;
using IOS.HMS.Service.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HMS.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class HMSService : IHMSService
    {
        public IEnumerable<Module> GetModuleDetails()
        {
            using (var dataContext = new HotelManagementDbContext())
            {
                var moduleRepository = new Repository<Module>(dataContext);
                return  moduleRepository.GetAll();
            }
        }
    }
}
