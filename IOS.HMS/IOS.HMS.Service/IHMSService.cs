﻿using HMS.Core.DomainObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HMS.Service
{

    [ServiceContract]
    public interface IHMSService
    {
        [OperationContract]
        IEnumerable<Module> GetModuleDetails();

    }

  
   
}
