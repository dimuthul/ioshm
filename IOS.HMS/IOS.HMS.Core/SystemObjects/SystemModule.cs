﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace IOS.HMS.Core.SystemObjects
{
    public delegate void ModuleMinimize(UserControl userControl);
    public delegate void ModuleMaximize(UserControl userControl);
    public abstract class  SystemModule
    {
        private int _iD = -1;
        [DataMember]
        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _displayName = string.Empty;
        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private string _color = string.Empty;
        [DataMember]
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
        private string _smallIcon = string.Empty;
        [DataMember]
        public string SmallIcon
        {
            get { return _smallIcon; }
            set { _smallIcon = value; }
        }
        private string _largeIcon = string.Empty;
        [DataMember]
        public string LargeIcon
        {
            get { return _largeIcon; }
            set { _largeIcon = value; }
        }

        private ToolTip _toolTip = new ToolTip();

        public ToolTip ToolTip
        {
            get { return _toolTip; }
            set { _toolTip = value; }
        }
        private UserControl _userControl = new UserControl();

        public UserControl UserControl
        {
            get { return _userControl; }
            set { _userControl = value; }
        }

        public event ModuleMinimize MinimizeEvent;

        public event ModuleMaximize MaximizeEvent;

        
    }
}
