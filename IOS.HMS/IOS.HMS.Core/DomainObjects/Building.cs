﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Building
    {
        public int  Id{ get; set; }
        public int BranchId { get; set; }
        public string Name { get; set; }
        private List<Floor> _floorList = new List<Floor>();

        public List<Floor> FloorList
        {
            get { return _floorList; }
            set { _floorList = value; }
        }
    }
}
