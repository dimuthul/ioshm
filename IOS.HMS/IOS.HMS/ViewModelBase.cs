﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace HMS
{
    public class ViewModelBase : DependencyObject, INotifyPropertyChanged
    {
        protected void OnNotifyPropertyChanged(string tprop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(tprop));
            }
        }
        public bool IsDesignTime
        {
            get
            {
                return (Application.Current == null) || (Application.Current.GetType() == typeof(Application));
            }
        }

        protected void InFormAsynchCallComplete()
        {
            if (AsynCallCompleted != null)
            {
                AsynCallCompleted(this, new AsyncCompletedEventArgs(null, false, null));
            }
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        public event AsyncCompletedEventHandler AsynCallCompleted;
        #endregion


    }
}
