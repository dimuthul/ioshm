﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace IOS.HMS.Core.SystemObjects
{
    public interface IView
    {

         int Id { get; set; }
         string Name { get; set; }
         string DisplayName { get; set; }
         string Discription { get; set; }
         string Title { get; set; }
         //UserControl UserContol { get; set; }
      
    }
}
