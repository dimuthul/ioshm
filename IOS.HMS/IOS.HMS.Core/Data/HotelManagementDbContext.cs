﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using HMS.Core.DomainObjects;

namespace HMS.Core
{
    public class HotelManagementDbContext : DbContext
    {
        public HotelManagementDbContext()
            :base("Server=.;;Database=IOSHMS;Integrated Security=True;")

        {
            Database.SetInitializer<HotelManagementDbContext>(null);
            //DbMigrationsConfiguration.AutomaticMigrationsEnabled = true;
        }
        public DbSet<Company> CompanyInfo { get; set; }
        public DbSet<Department> Departments{ get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Floor> Floors { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Service> Services { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Login> Logins { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Operation> Operations { get; set; }
        public DbSet<SubOperation> SubOperations { get; set; }

    }
}
