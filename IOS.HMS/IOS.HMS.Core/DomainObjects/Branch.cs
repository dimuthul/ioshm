﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Branch
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string RegNo { get; set; }
        public string Email { get; set; }
        public string ImageUrl { get; set; }
        public string Url { get; set; }
        public bool Selected { get; set; }
        private List<Building> _buildingList = new List<Building>();

        public List<Building> BuildingList
        {
            get { return _buildingList; }
            set { _buildingList = value; }
        }
    }
}
