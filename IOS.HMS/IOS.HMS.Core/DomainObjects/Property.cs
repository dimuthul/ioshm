﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public abstract class Property
    {
        public int PropertyId { get; set; }
        //private List<PropertyAttribute> _attributeList = new List<PropertyAttribute>();

        //public List<PropertyAttribute> AttributeList
        //{
        //    get { return _attributeList; }
        //    set { _attributeList = value; }
        //}
        public string Name { get; set; }
        public string Description { get; set; }
        private List<string> _imageList = new List<string>();
        public List<string> ImageList
        {
            get { return _imageList; }
            set { _imageList = value; }
        }
        private List<Property> _addonFacilityList = new List<Property>();
        public List<Property> AddonFalilityList
        {
            get { return _addonFacilityList; }
            set { _addonFacilityList = value; }
        }
        private List<Property> _addonFacilityFor = new List<Property>();

        public List<Property> AddonFacilityFor
        {
            get { return _addonFacilityFor; }
            set { _addonFacilityFor = value; }
        }
        public bool AbleToBook { get; set; }
        public bool Booked { get; set; }
        public DateTime BookFromDateTime { get; set; }
        public DateTime BookToDateTime { get; set; }
    }
}
