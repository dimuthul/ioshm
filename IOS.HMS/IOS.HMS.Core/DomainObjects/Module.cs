﻿using IOS.HMS.Core.SystemObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    [DataContract]
    public class Module : SystemModule
    {
        private int  _iD = -1;
        [DataMember]
        public int ID
        {
          get { return _iD; }
          set { _iD = value; }
        }
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _displayName = string.Empty;
        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private string _color = string.Empty;
        [DataMember]
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
         private string _smallIcon = string.Empty;
         [DataMember]
         public string SmallIcon
         {
             get { return _smallIcon; }
             set { _smallIcon = value; }
         }
        private string _largeIcon = string.Empty;
        [DataMember]
        public string LargeIcon
        {
            get { return _largeIcon; }
            set { _largeIcon = value; }
        }

        private List<Operation> _operationList = new List<Operation>();
        [DataMember]
        public List<Operation> OperationList
        {
            get { return _operationList; }
            set { _operationList = value; }
        }



    }
}
