﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Operation
    {
        private int _iD = -1;

        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }
        private int _moduleId = -1;

        public int ModuleId
        {
            get { return _moduleId; }
            set { _moduleId = value; }
        }

        private string _name = string.Empty;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _displayName = string.Empty;

        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private string _color = string.Empty;

        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
        private string _icon = string.Empty;

        public string Icon
        {
            get { return _icon; }
            set { _icon = value; }
        }
        private List<Operation> _subOperationList = new List<Operation>();

        public List<Operation> SubOperationList
        {
            get { return _subOperationList; }
            set { _subOperationList = value; }
        }

    }
}
