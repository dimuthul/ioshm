﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Booking
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int NoOfAttendies { get; set; }
        public bool RepeatBooking { get; set; }
        private DateTime _bookingStartDate = new DateTime (1900,1,1);

        public DateTime BookingStartDate
        {
            get { return _bookingStartDate; }
            set { _bookingStartDate = value; }
        }
        [NotMapped]
        public int StartBookingYear {
            get
            {
                return BookingStartDate.Year;
            }
            set
            {
                
            }
        }
        [NotMapped]
        public int StartBookingMonth
        {
            get
            {
                return BookingStartDate.Month;
            }
            set
            {

            }
        }
        [NotMapped]
        public int StartBookingDay
        {
            get
            {
                return BookingStartDate.Day;
            }
            set
            {

            }
        }
        [NotMapped]
        public int StartBookingHour
        {
            get
            {
                return BookingStartDate.Hour;
            }
            set
            {

            }
        }
        [NotMapped]
        public int EndBookingYear
        {
            get
            {
                return BookingEndDate.Year;
            }
            set
            {

            }
        }
        [NotMapped]
        public int EndBookingMonth
        {
            get
            {
                return BookingEndDate.Month;
            }
            set
            {

            }
        }
        [NotMapped]
        public int EndBookingDay
        {
            get
            {
                return BookingEndDate.Month;
            }
            set
            {

            }
        }
        [NotMapped]
        public int EndBookingHour
        {
            get
            {
               
                return BookingEndDate.Hour;
            }
            set
            {

            }
        }
        private DateTime _bookingEndDate = new DateTime(1900, 1, 1);

        public DateTime BookingEndDate
        {
            get { return _bookingEndDate; }
            set { _bookingEndDate = value; }
        }
       
        public string PurposeOfBooking { get; set; }
        public string BookingDescription { get; set; }
        [NotMapped]
        private List<Employee> _internalAttedeeList = new List<Employee>();
        [NotMapped]
        public List<Employee> InternalAttedeeList
        {
            get { return _internalAttedeeList; }
            set { _internalAttedeeList = value; }
        }
        [NotMapped]
        private List<Service> _serviceList = new List<Service>();
        [NotMapped]
        public List<Service> ServiceList
        {
            get { return _serviceList; }
            set { _serviceList = value; }
        }

      
        public string ContactNo { get; set; }
        [NotMapped]
        public decimal TotalPayment { get; set; }
        
    }
}
