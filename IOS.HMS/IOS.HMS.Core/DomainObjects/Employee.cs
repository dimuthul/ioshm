﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Employee
    {
        public int Id { get; set; }
        [NotMapped]
        public string Name
        {
            get
            {
                return FirstName;
            }
            set
            {
                //Nothing to do here..
            }
        }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Category { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
        private List<Department> _departmentList = new List<Department>();

        public List<Department> DepartmentList
        {
            get { return _departmentList; }
            set { _departmentList = value; }
        }
        private List<Address> _addressList = new List<Address>();

        public List<Address> AddressList
        {
            get { return _addressList; }
            set { _addressList = value; }
        }
        public string HomeNumber { get; set; }
        public string OfficeNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
   

        public bool Selected { get; set; }
        public string StaffNo { get; set; }
        public string AccessCardNo { get; set; }
        public string ImageUrl { get; set; }

        [NotMapped]
        private List<Service> _serviceList = new List<Service>();
        [NotMapped]
        public List<Service> ServiceList
        {
            get { return _serviceList; }
            set { _serviceList = value; }
        }

      

        public int BranchId { get; set; }

    }
}
