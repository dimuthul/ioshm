﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace IOS.HMS.Core.SystemObjects
{
    public delegate void FeatureMinimize(UserControl userControl);
    public delegate void FeatureMaximize(UserControl userControl);
   
    public abstract class  SystemFeature
    {
        private int _iD = -1;

        public int ID
        {
            get { return _iD; }
            set { _iD = value; }
        }
        private string _name = string.Empty;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _displayName = string.Empty;

        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private string _color = string.Empty;

        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
        private string _smallIcon = string.Empty;

        public string SmallIcon
        {
            get { return _smallIcon; }
            set { _smallIcon = value; }
        }

        private ToolTip _toolTip = new ToolTip();

        public ToolTip ToolTip
        {
            get { return _toolTip; }
            set { _toolTip = value; }
        }

        private string _largeIcon = string.Empty;

        public string LargeIcon
        {
            get { return _largeIcon; }
            set { _largeIcon = value; }
        }
        private UserControl _userControl = new UserControl();

        public UserControl UserControl
        {
            get { return _userControl; }
            set { _userControl = value; }
        }

        public event FeatureMinimize MinimizeEvent;

        public event FeatureMaximize MaximizeEvent;

        
    }
}

