﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Floor
    {
        public int Id { get; set; }
        public int BuildingId { get; set; }
        public string Name { get; set; }
        private List<Room> _roomList = new List<Room>();

        public List<Room> RoomList
        {
            get { return _roomList; }
            set { _roomList = value; }
        }
    }
}
