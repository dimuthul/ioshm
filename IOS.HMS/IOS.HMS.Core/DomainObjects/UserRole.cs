﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class UserRole
    {
        public int id { get; set; }
        public string RoleName { get; set; }
    }
}
