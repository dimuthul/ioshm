﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ServiceCategoryId { get; set; }
        public decimal Charge { get; set; }
        public Department ServiceDeparment { get; set; }
        public bool Selected { get; set; }
        public int Units { get; set; }
        public int ChargingTypeId { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Total 
        {
            get
            {
                return Units * UnitPrice;
            }
            set
            {

            }
        }
		public string ImageUrl { get; set; }
		
        [NotMapped]
        private List<Branch> _branchList = new List<Branch>();
        [NotMapped]
        public List<Branch> BranchList
        {
            get { return _branchList; }
            set { _branchList = value; }
        }

        
    }
}
