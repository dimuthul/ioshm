﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Company
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        private List<Branch> _hotelList = new List<Branch>(); 

        public List<Branch> HotelList
        {
            get { return _hotelList; }
            set { _hotelList = value; }
        }
    }
}
