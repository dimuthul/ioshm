﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.DomainObjects
{
    public class Room
    {
        public int Id { get; set; }
        public decimal Area { get; set; }
        public int FloorId { get; set; }
        public int NoOfUnits { get; set; }
        public bool Smoking { get; set; }
        public int RoomTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NoOfPeopleAllowed { get; set; }
        private List<string> _imageList = new List<string>();
        public List<string> ImageList
        {
            get { return _imageList; }
            set { _imageList = value; }
        }
        private List<Service> _addonFacilityList = new List<Service>();
        public List<Service> AddonFalilityList
        {
            get { return _addonFacilityList; }
            set { _addonFacilityList = value; }
        }
       
        public bool AbleToBook { get; set; }
        public bool Booked { get; set; }
        private DateTime _bookFromDateTime = DateTime.Now;

        public DateTime BookFromDateTime
        {
            get { return _bookFromDateTime; }
            set { _bookFromDateTime = value; }
        }
        private DateTime _bookToDateTime = DateTime.Now;

        public DateTime BookToDateTime
        {
            get { return _bookToDateTime; }
            set { _bookToDateTime = value; }
        }
        public bool OnMaintenance { get; set; }
        [NotMapped]
        public string ChargingTypeText { get; set; }
        [NotMapped]
        public decimal UnitPrice { get; set; }
    }
}
